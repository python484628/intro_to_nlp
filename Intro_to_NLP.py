
# Author : Marion Estoup
# E-mail : marion_110@hotmail.fr
# April 2023

"""**BERT FINE TUNING**

Text Classification

BERT Bidirectional Encoder Representations from Transformers. It's a method of pre-training for linguistic representations
"""

# Install library
!pip install datasets

# Import data
# Link : https://huggingface.co/datasets/solomonk/reddit_mental_health_posts
from datasets import load_dataset

dataset = load_dataset("solomonk/reddit_mental_health_posts")

# Display the first rows of our data
first_example = dataset['train'][0]
first_example

import pandas as pd

# Convert dataset to a pandas dataframe
df = pd.DataFrame(dataset['train'])

# Save dataframe as csv file
df.to_csv('dataset.csv', index=False)

# Load the dataset
df = pd.read_csv('dataset.csv')

# Report the number of sentences
print('Number of training sentences: {:,}\n'.format(df.shape[0]))

# Display 10 random rows from the data
df.sample(10)

"""The two variables that we are interested in are Body(text) and Subreddit(label)

We can display 5 sentences that belong to the class "depression" with the following command :
"""

df.loc[df.subreddit == 'depression'].sample(5)[['body','subreddit']]

"""#Clean text

There are some "useless" information such as emojis, URLs, ... So we can clean the text before training the model

Remove rows where we don't have text (it's written removed, deleted, nan, ...)
"""

# Let's see if there are any null values present in our dataset
df.isnull().sum()

# Let's drop the null values
df.dropna(inplace=True)
df.isnull().sum()

df.sample(10) # We still have [deleted] and [removed]

# Filter and remove rows
df = df[(df['body'] != 'deleted') & (df['body'] != 'removed') & (~df['body'].isna())]

"""Remove URLs, emojis, tags and punctuation"""

# Import libraries
import string
import re

from pygments.token import Text
# Helper functions to clean text by removing urls, emojis, html tags and punctuations
def remove_URL(text):
    url = re.compile(r'https?://\S+|www\.\S+')
    return url.sub(r'', text)

def remove_emoji(text):
    emoji_pattern = re.compile(
        '['
        u'\U0001F600-\U0001F64F'  # emoticons
        u'\U0001F300-\U0001F5FF'  # symbols & pictographs
        u'\U0001F680-\U0001F6FF'  # transport & map symbols
        u'\U0001F1E0-\U0001F1FF'  # flags (iOS)
        u'\U00002702-\U000027B0'
        u'\U000024C2-\U0001F251'
        ']+',
        flags=re.UNICODE)
    return emoji_pattern.sub(r'', text)


def remove_html(text):
    html = re.compile(r'<.*?>|&([a-z0-9]+|#[0-9]{1,6}|#x[0-9a-f]{1,6});')
    return re.sub(html, '', text)


def remove_punct(text):
  # remove punctuations
    table = str.maketrans('', '', string.punctuation)
    return text.translate(table)

def remove_newlines(text):
  # Remove newlines and extra whitespaces
    text = re.sub(r'\n', ' ', text)
    text = re.sub(r'\s+', ' ', text).strip()
    return text
def remove_digits(text):
  # remove numbers and digits
  text = re.sub(r'\w*\d\w*', '', text) # Or maybe \d+ could work
  return text

df['body'] = df['body'].apply(lambda x:remove_URL(x))
df['body'] = df['body'].apply(lambda x:remove_emoji(x))
df['body'] = df['body'].apply(lambda x:remove_html(x))
df['body'] = df['body'].apply(lambda x:remove_punct(x))
df['body'] = df['body'].apply(lambda x:remove_digits(x))

# More cleaning
def remove_repetitive_letters(text):
    pattern = r'(\w)\1{2,}'  # Matches a letter repeated more than 2 times
    cleaned_text = re.sub(pattern, r'\1', text)
    return cleaned_text

df['body'] = df['body'].apply(lambda x: remove_repetitive_letters(x))

# Dictionary of English Contractions
contractions_dict = { "ain't": "are not","'s":" is","aren't": "are not",
                     "can't": "cannot","can't've": "cannot have",
                     "'cause": "because","could've": "could have","couldn't": "could not",
                     "couldn't've": "could not have", "didn't": "did not","doesn't": "does not",
                     "don't": "do not","hadn't": "had not","hadn't've": "had not have",
                     "hasn't": "has not","haven't": "have not","he'd": "he would",
                     "he'd've": "he would have","he'll": "he will", "he'll've": "he will have",
                     "how'd": "how did","how'd'y": "how do you","how'll": "how will",
                     "I'd": "I would", "I'd've": "I would have","I'll": "I will",
                     "I'll've": "I will have","I'm": "I am","I've": "I have", "isn't": "is not",
                     "it'd": "it would","it'd've": "it would have","it'll": "it will",
                     "it'll've": "it will have", "let's": "let us","ma'am": "madam",
                     "mayn't": "may not","might've": "might have","mightn't": "might not",
                     "mightn't've": "might not have","must've": "must have","mustn't": "must not",
                     "mustn't've": "must not have", "needn't": "need not",
                     "needn't've": "need not have","o'clock": "of the clock","oughtn't": "ought not",
                     "oughtn't've": "ought not have","shan't": "shall not","sha'n't": "shall not",
                     "shan't've": "shall not have","she'd": "she would","she'd've": "she would have",
                     "she'll": "she will", "she'll've": "she will have","should've": "should have",
                     "shouldn't": "should not", "shouldn't've": "should not have","so've": "so have",
                     "that'd": "that would","that'd've": "that would have", "there'd": "there would",
                     "there'd've": "there would have", "they'd": "they would",
                     "they'd've": "they would have","they'll": "they will",
                     "they'll've": "they will have", "they're": "they are","they've": "they have",
                     "to've": "to have","wasn't": "was not","we'd": "we would",
                     "we'd've": "we would have","we'll": "we will","we'll've": "we will have",
                     "we're": "we are","we've": "we have", "weren't": "were not","what'll": "what will",
                     "what'll've": "what will have","what're": "what are", "what've": "what have",
                     "when've": "when have","where'd": "where did", "where've": "where have",
                     "who'll": "who will","who'll've": "who will have","who've": "who have",
                     "why've": "why have","will've": "will have","won't": "will not",
                     "won't've": "will not have", "would've": "would have","wouldn't": "would not",
                     "wouldn't've": "would not have","y'all": "you all", "y'all'd": "you all would",
                     "y'all'd've": "you all would have","y'all're": "you all are",
                     "y'all've": "you all have", "you'd": "you would","you'd've": "you would have",
                     "you'll": "you will","you'll've": "you will have", "you're": "you are",
                     "you've": "you have"}

# Regular expression for finding contractions
contractions_re = re.compile('(%s)' % '|'.join(contractions_dict.keys()))

# Function for expanding contractions
def expand_contractions(text,contractions_dict=contractions_dict):
  def replace(match):
    return contractions_dict[match.group(0)]
  return contractions_re.sub(replace, text)

# Expanding Contractions in the reviews
df['body'] = df['body'].apply(lambda x: expand_contractions(x))

"""#Remove stopwords

Stopwords are words that are not considered as interesting for our study (don't bring additional or important information to the text).
"""

import re
import string
import nltk
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
nltk.download('stopwords')
nltk.download('punkt')

def clean_text(text):

  # Convert to lowercase
  text = text.lower()

  # Remove stopwords
  stop_words = set(stopwords.words('english'))
  tokens = word_tokenize(text)
  text = ' '.join([token for token in tokens if token not in stop_words])

  return text


df['body'] = df['body'].apply(lambda x:clean_text(x))

df.loc[df.subreddit == 'OCD'].sample(5)[['body','subreddit']]

"""#EDA : Exploratory Data Analysis

Data Distribution
"""

import matplotlib.pyplot as plt

# Count the occurrence of each label
label_counts = df['subreddit'].value_counts()

# Get the labels and their counts
labels = label_counts.index.tolist()
counts = label_counts.tolist()

# Plot the data distribution
plt.bar(labels, counts, color='green')

# Add labels and title
plt.xlabel('Label')
plt.ylabel('Count')
plt.title('Data Distribution')

# Show the plot
plt.show()

# Calculate word count for each tweet
df['word_count'] = df['body'].str.split().apply(len)

# Group the data by disease and calculate the average word count per disease
disease_word_count = df.groupby('subreddit')['word_count'].mean()

# Plot the word count per tweet per category
disease_word_count.plot(kind='bar', figsize=(10, 6), color='purple')
plt.xlabel('Disease')
plt.ylabel('Average Word Count per Post')
plt.title('Word Count per Post per Disease')
plt.show()

"""Document Term Matrix (matrix that gives words frequencies in a text)"""

# Let's group our cleaned texts by subreddits in a new dataframe
df_grouped = df[['subreddit','body']].groupby(by='subreddit').agg(lambda x:' '.join(x))
df_grouped.head()

# Creating Document Term Matrix
from sklearn.feature_extraction.text import CountVectorizer

cv = CountVectorizer(analyzer='word')
data = cv.fit_transform(df_grouped['body'])
df_dtm = pd.DataFrame(data.toarray(), columns = cv.get_feature_names_out())
df_dtm.index = df_grouped.index

df_dtm.head()

"""#Wordclouds"""

# Importing wordcloud for plotting word clouds and textwrap for wrapping longer text
from wordcloud import WordCloud
from textwrap import wrap

# Function for generating word clouds
def generate_wordcloud(data, title):
  wc = WordCloud(width=400, height=330, max_words=150,colormap="Dark2").generate_from_frequencies(data)
  plt.figure(figsize=(10,8))
  plt.imshow(wc, interpolation='bilinear')
  plt.axis("off")
  plt.title('\n'.join(wrap(title,60)),fontsize=13)
  plt.show()

# Transposing document term matrix
df_dtm = df_dtm.transpose()

# Plotting word cloud for each label
for index, label in enumerate(df_dtm.columns):
  generate_wordcloud(df_dtm[label].sort_values(ascending=False),label)

"""**Polarity (Sentiment) Analysis**"""

# Import library
from textblob import TextBlob

# Polarity analysis
df['polarity'] = df['body'].apply(lambda x:TextBlob(x).sentiment.polarity)

# let's take a look at some posts
print("3 Random posts with Highest Polarity:")
for index,post in enumerate(df.loc[df['polarity'].sort_values(ascending=False)[:3].index]['body']):
  print('Post {}:\n'.format(index+1),post)

print("3 Random posts with Lowest Polarity:")
for index,review in enumerate(df.loc[df['polarity'].sort_values(ascending=True)[:3].index]['body']):
  print('Post {}:\n'.format(index+1),review)

import numpy as np
# Let's plot polarity of posts for each mental disease
post_polarity_sorted = pd.DataFrame(df.groupby('subreddit')['polarity'].mean().sort_values(ascending=True))

plt.figure(figsize=(16,8))
plt.xlabel('Polarity')
plt.ylabel('Diseases')
plt.title('Polarity of Different Reddit Mental Health posts')
polarity_graph = plt.barh(np.arange(len(post_polarity_sorted.index)),post_polarity_sorted['polarity'],color='purple',)

# Writing product names on bar
for bar,disease in zip(polarity_graph,post_polarity_sorted.index):
  plt.text(0.005,bar.get_y()+bar.get_width(),'{}'.format(disease),va='center',fontsize=11,color='black')

# Writing polarity values on graph
for bar,polarity in zip(polarity_graph,post_polarity_sorted['polarity']):
  plt.text(bar.get_width()+0.001,bar.get_y()+bar.get_width(),'%.3f'%polarity,va='center',fontsize=11,color='black')

plt.yticks([])
plt.show()
